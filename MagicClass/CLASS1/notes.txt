NOTES
(SPIROGRAPH IS STILL RUNNING) 
Hello. This is a spirograph that I wrote to show off what you can use programming for. I won�t get into how you would write this in this class. But I *suppose* you could probably figure it out. It�s not that hard. I guess you could probably change it � make it do more of the pretty ones, and fewer of the ugly ones, more colors you like and fewer you don�t like.  I guess that�s what we�re here for today � to take you to the door of programming ; you can go through if you want. 
I guess we should get started.
My name is Dave Menconi.
I was a game developer back when it was uncool. Um, and then when it was cool. And then when it was uncool again. Is game development cool or uncool these days?
I�m here to give you a taste of programming, just to see if you might like it.
How many people have had a chance to program a computer; not use it but actually program it? Well, maybe after today, all the hands can go up.

If you love it and it's easy, you already know it or can find other resources; nothing to see here
If you hate it and it's hard, that's good to know but you probably should do something else
if you Love it but it's hard, maybe we can help make it easier
If you hate it but it's easy, maybe we can help make it more fun
The point is, it would be good to know, right?

There are no grades in this class. You are the only one to say how you do. 
Winning is not about being a great programmer.
Winning is learning what you think about programming and learning more if you like it.

I�m going to explain some things. Then I�m going to show you how to use those things in some examples I have. 
But you�re not going to know how to program until you try it yourself.  
The handout has a few examples that you can try yourself. THEN you�ll be a programmer!

Asking questions is good. If you don�t understand something I say, please ask. Don�t be shy � my wife doesn�t know what I�m talking about either and she has a Masters Degree.
But if you�re not really in a place to ask, like if you�re looking at a program at home, don�t get stuck. Keep going � you�ll probably figure it out. 
It�s like when you�re reading and you don�t know a word. Maybe you stop every time you see a new word and ask about it but it sure saves a lot of time if you figure it out from context. Works the same way with programs.
Besides, programming is really just a bunch of Magic Words strung together. Almost no one knows how they work, just what the seem to do. Just copy them verbatim and have faith in the Magic.
Of course if you�re curious the web has tons of people who actually KNOW (the people who wrote the language we�ll be learning, for example).
By the way, that�s our language right there: python. It�s not as big as C or Java or even PHP but its more fun and it will be good for us. And you can do anything with Python that you can do with those other languages, pretty much.
You can just dabble your toes but if you want to, you can swim with the best of them with Python.

Oh, now sorry, wrong kind of handout.
I meant the class notes on this piece of paper.
It has lots of useful stuff on it; it�s the key to the door into programming.  It also has my contact information if you want to ask some questions. And links to some example programs � including the spyrograph program and a little dungeon game � that you can run yourself and modify. 
If you want to learn programming, or just have seom fun that�s the trick: modify some programs that sort of work to make them work for you. It�s fun, it�s easy and it teach you to program faster than anything else you can do.
