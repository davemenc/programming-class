import pygame, sys
from pygame.locals import *
import random
import math
# set up the window
pygame.init()
ScreenX = 1300
ScreenY = 750
DISPLAYSURF = pygame.display.set_mode((ScreenX,ScreenY),pygame.FULLSCREEN)
pygame.display.set_caption('Spirograph')
DISPLAYSURF.fill((255, 255, 255))
LINEWIDTH = 5

# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)
CYAN =  (  0, 255, 255)
MAGENTA =(255,  0, 255)
ORANGE =(255,  69,   0)
AQUAMARINE = (102,205,170)
NAVY = (0,0,128)
BLUEVIOLET = (138,43,226)
DEEPPINK=(255,20,147)
YELLOW=(255,255,0)
OLIVEGREEN= (85,107,47)
YELLOWGREEN=(154,205,50)
colors = [ RED, GREEN, BLUE,CYAN,MAGENTA,ORANGE,AQUAMARINE,NAVY,BLUEVIOLET,DEEPPINK,YELLOW,OLIVEGREEN,YELLOWGREEN]

DRAWINGS=0
TotalDraws = 0
Linewidth = 1
while TotalDraws< DRAWINGS or DRAWINGS==0:
	TotalDraws+=1
	Linewidth = int((TotalDraws%75)/10+1)
	color = colors[random.randrange(len(colors))]
	centerX = random.randrange(100,ScreenX-100)
	centerY = random.randrange(100,ScreenY-100)
	ringR = random.randrange(10,500)
	orbitR = random.randrange(10,100)
	startOrbitAngle = random.randrange(0,3600)/10.0
	startRingAngle = random.randrange(0,3600)/10.0
	totalPoints = random.randrange(2500,5000)
	RingAngleVelocity = float(random.randrange(200,1000))/100.0 #how fast around the ring -- faster than orbit
	OrbitAngleVelocity = float(random.randrange(6,200))/100.0 #how fast around the orbit -- slower
	RingAngleVelocity = 2
	#OrbitAngleVelocity = .5
	#print " centerX: {0}\n centerY: {1}\n ringR: {2}\n orbitR: {3}\n startOrbitAngle: {4}\n startRingAngle: {5}\n totalPoints: {6}\n RingAngleVelocity: {7}\n OrbitAngleVelocity: {8}\n ScreenX: {9}\n ScreenY: {10}\n ".format(centerX,centerY,ringR,orbitR,startOrbitAngle,startRingAngle,totalPoints,RingAngleVelocity,OrbitAngleVelocity,ScreenX,ScreenY)
	OrbitAngle = startOrbitAngle
	RingAngle = startRingAngle
	OldOrbitX = None
	OldOrbitY = None
	RingX = None
	RingY = None
	OrbitX = None
	OrbitY = None
	for i in range(0,totalPoints):
#		print "i: {0}; RingX:{1}; RingY: {2}; OrbitX: {3}; OrbitY: {4}; OldOrbitX: {5}; OldOrbitY: {6}; OrbitAngle:{7}; RingAngle:{8}".format(i,RingX,RingY,OrbitX,OrbitY,OldOrbitX, OldOrbitY,OrbitAngle,RingAngle)
		RingX = centerX + math.cos(math.radians(RingAngle))*ringR
		RingY = centerY + math.sin(math.radians(RingAngle))*ringR
		OrbitX = int(round(RingX + math.cos(math.radians(OrbitAngle))*orbitR))
		OrbitY = int(round(RingY + math.sin(math.radians(OrbitAngle))*orbitR))
		if OldOrbitX != None and OldOrbitY != None:
			if OldOrbitX > 0 and OldOrbitY > 0 and OrbitX > 0 and OrbitY > 0 and OldOrbitX < ScreenX and OldOrbitY < ScreenY and OrbitX< ScreenX and OrbitY < ScreenY:
				pygame.draw.line(DISPLAYSURF, color, (OldOrbitX, OldOrbitY), (OrbitX, OrbitY), Linewidth)
				pygame.display.update()
#				print "drawing from {0},{1} to {2},{3} ".format(OldOrbitX,OldOrbitY,OrbitX,OrbitY)
		
		OrbitAngle += OrbitAngleVelocity
		RingAngle += RingAngleVelocity
		OldOrbitX = OrbitX
		OldOrbitY = OrbitY
raw_input()