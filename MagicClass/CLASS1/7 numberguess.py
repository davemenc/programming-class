import random

print "This is a number guessing game."
print "I will think of a number between 1 and 10."
print "You have 3 chances to guess it."

number = random.randrange(1,11)

for i in range(1,4):
	guess = int(raw_input("Guess my number: "))
	if number==guess:
		print "Right, it was {0}. \nYou got it on try {1}! \nCongratulations!".format(number,i)
		break
	else:
		print "No, sorry. My number is not {0}.".format(guess)
if number!=guess:
	print "That was 3 guesses. "
	print "My number was {0}. ".format(number)
	print "Better luck next time. "

raw_input()