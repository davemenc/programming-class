This is the outline for one single class with no follow up classes

* Spirograph demo -- spirograph demo is running and I'm watching it as if mesmerized
	** I won't teach you how to do this
	** but I guess you will understand how it works -- if you wanted to
	** and I guess you could modify -- if you wanted to
	** that's what this is about: taking you to the threshold of programming
	** step in or walk away; that's up to you
* Name & purpose
	** game dev when it was uncool; then cool; then uncool -- what is it now?
* What this class is for: do you like programming?

				easy | Hard
				-------------
			Hate | 	Yes  |  NO!
			---------------------
			Love | 	no   |  Yes
	** If you love it and it's easy, you already know it or can find other resources; nothing to see here
	** If you hate it and it's hard, that's good to know but you probably should do something else
	** if you Love it but it's hard, maybe we can help make it easier
	** If you hate it but it's easy, maybe we can help make it more fun
* Winning this class
	** There are no grades; no one but you can say how you did
	** Winning is not about being the worlds greatest programmer
	** Winning is about knowing how you feel and what talents you have for programming
* Structure of this class
	** explain it all
	** do it all
	** But you�re not going to know how to program until you try it yourself.  
* A word about learning technical stuff
	** don't be afraid to ask -- no one else knows what I'm talking about either
	** don't get stuck -- if you don't understand try ignoring it and seeing what else you can figure out; like figuring out a word from context while reading
	** you can program without knowing what you are doing; most programmers are doing just that
		*** most of the program is a bunch of "magic statements" that just work; copy them verbatim and don't worry about it
		*** try to be curious and, when you have the time, find out what those words do -- the internet has all this information
* The handout
	** How to download python
	** How to get the programs/videos
	** How to take an online class
	** Where the documentation is
	** How to contact me
* The 5 Python commands
	** print -- displays stuff on the screen -- print "Then answer is ", answer
		*** string in quotes
		*** commas separate things
		*** commas generate a space
	** = -- variable assignment -- answer = 6 or answer = "six" or answer = 6.0
		*** int
		*** str
		*** float
		*** other types
		*** variables can be any type but constants are only one and the variable takes on that type
	** raw_input -- get something from the user  -- answer = raw_input("WHAT?")
		*** puts in a variable
		*** always a string
	** if -- branching, conditional -- if answer == 6: or if True: or if ansnwer > 7:
		*** True
		*** False
		*** ==
		*** !=
		*** >
		*** <
	** for -- looping, iteration -- for i in range(0,10):
		*** variable
		*** range
* The 7 demo programs
	** 1 helloworld.py -- demo the print command
		*** notice the quotes
		*** ignore the "raw_input" at the end
	** 2 hellodave.py -- set a variable
		*** "person" is the variable
		*** it is of type str
		*** notice the small quotes
		*** ignore the raw_input() again
	** 3 helloinput.py -- raw_input demo
		*** NOW we see the raw_input
		*** Notice the string in the parens
		*** notice that person gets the results
		*** person is type str
	** 4 helloif.py -- if demo
		*** notice the if statment
		*** compares person to 'Dave'
		*** notice ==
		*** notice the block
		*** elif -- try again
		*** else -- do this if nothing else works
		*** notice that it ALWAYS prints the last message
	** 5 password.py -- for and everything we've learned
		*** for statement -- just steps through 0-3
		*** gets a password
		*** checks the password
		*** if good, sets success
		*** Note break
		*** otherwise loops
		*** check for success
		*** othwerwise kicks you out
		*** This is NOT the way real password programs work (although if they can't read code...)
	** 6 guess6.py -- student is rick rolled (six rolled)
		*** Call for a volunteer
		*** run the program
		*** let them stuggle a bit
		*** prompt them to accept 6
		*** Youve been Six Rolled (tm)
		*** run again but this time click on exit
		*** go through program line by line
	** 7 numberguess.py -- a real number guessing game
		*** import random
		*** randrange()
		*** 3 tries
		*** check for correct guess
		** loop (or not) -- break
		** after they have 3 tries, we tell them
* Goodbye and good luck
	** Show of hands: how many will go home and download python?
	** I bet you'll all be playing angry birds from the minute you turn on your computer!
	** But I recommend that you go to this "learn python" site and do a few lessons tonight. 
		*** That's how I learned python
		*** It's really quick
		*** You have a big leg up because of our work here
		*** You'll probably even enjoy it
	** who knows, maybe a year from now you'll be teaching me how to program!
