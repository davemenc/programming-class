import random

def RollSixSided():
	return random.randrange(6)+1

def CreateChar(name,boost):
	global CharStrength, CharSize, CharLuck, CharCharisma, CharStamina, CharGold, CharName, AttributeEffect
	#Attributes (will normally be 3-18 but enhanced stat is 4-24)
	CharStrength = 0 
	CharSize = 0
	CharLuck = 0
	CharCharisma = 0
	CharStamina = 0
	CharGold = 10
	CharName = ""

	CharLuck = RollSixSided()+RollSixSided()+RollSixSided()
	CharStrength = RollSixSided()+RollSixSided()+RollSixSided()
	CharSize = RollSixSided()+RollSixSided()+RollSixSided()
	CharCharisma = RollSixSided()+RollSixSided()+RollSixSided()
	if boost == 1:
		CharStrength += RollSixSided() 
		CharName = "The Mighty "+name
	elif boost == 2:
		CharSize += RollSixSided()
		CharName = "Big "+name
	elif boost == 3:
		CharLuck += RollSixSided()
		CharName = "Lucky "+name
	elif boost == 4:
		CharCharisma += RollSixSided()
		CharName = "My Friend "+name
	elif boost > 4:
		print "That's not a good boost number!"
		CharName = name + " The Foolish"
		CharGold += -boost+4
	elif boost < 1:
		print "That's not a good boost number!"
		CharName = name + " The Lame"
		CharGold += boost-1
	
	#now add in a bit for luck
	LuckBoost = int(round(CharLuck*AttributeEffect))
	CharStrength += random.randrange(LuckBoost)
	CharSize += random.randrange(LuckBoost)
	CharCharisma += random.randrange(LuckBoost)
	CharStamina = int(round(CharSize*AttributeEffect)) + 2 # this is your "hit points"
	
def DescribeChar():
	global CharStrength,CharSize,CharLuck,CharCharisma,CharStamina,CharGold,CharName,AttributeEffect
	
	print
	print CharName
	print "Str:\t{0}".format(CharStrength)
	print "Chr:\t{0}".format(CharCharisma)
	print "Size:\t{0}".format(CharSize)
	print "Luck:\t{0}".format(CharLuck)
	print "Gold:\t{0}".format(CharGold)
	print "Stam:\t{0}".format(CharStamina)
	print
def ActName(act):
	#print "ActName({0})".format(act)
	if act == "F":
		return "Fight"
	if act == "D":
		return "Drink"
	if act == "G":
		return "Gamble"
	if act == "R":
		return "Run"
	if act == "B":
		return "Bribe"

	print "Missing Action Name",act
	return "Unknown Action"

def AttName(att):
	#print "AttName({0})".format(att)
	att = str(att)
	if att == "1":
		return "Strength"
	elif att == "2":
		return "Size"
	elif att == "3":
		return "Luck"
	elif att == "4":
		return "Charisma"
	elif att == "5":
		return "Stamina"
	elif att == "6":
		return "Gold"
	else:
		print "Missing Attribute Name in AttName",att
		return "Unknown Attribute"

def DispResult (att,amt):
	attname = AttName(att)
	if amt>0:
		print "You gained {0} to your {1}.".format(amt,attname)
	if amt<0:
		print "You lost {0} from your {1}.".format(amt,attname)
	else:
		print "Your {0} remained the same.".format(attname)
	

def ChangeAtt(att,amt):
	global CharStrength, CharSize, CharLuck, CharCharisma, CharStamina, CharGold
	att = str(att)
	if att == "1":
		CharStrength+=amt
	elif att == "2":
		CharStrength+=amt
	elif att == "3":
		CharStrength+=amt
	elif att == "4":
		CharStrength+=amt
	elif att == "5":
		CharStrength+=amt
	elif att == "6":
		CharStrength+=amt
	elif att == "7":
		CharStamina+=amt
	else: 
		print "Missing Attribute in ChangeAtt",att
	#DispResult(att,amt)

def GetAtt(att):
	global CharStrength,CharSize,CharLuck,CharCharisma,CharStamina,CharGold
	att = str(att)
	if att == "1":
		return CharStrength
	elif att == "2":
		return CharSize
	elif att == "3":
		return CharLuck
	elif att == "4":
		return CharCharisma
	elif att == "5":
		return CharStamina
	elif att == "6":
		return CharGold
	else:
		print "Missing Attribute Name in ChangeAtt",att
		return 0

def DirName(move):
	global BATCH
	if BATCH:
		print "DirName({0})".format(move)
	if move == "N":
		return "North"
	elif move == "S":
		return "South"
	elif move == "E":
		return "East"
	elif move == "W": 
		return "West"
	elif move == "U":
		return "Up"
	elif move == "D":
		return "Down"
	print "Missing Direction Name",move
	return "Unknown Direction"

def DescribeRoom(roomname,monster,movechars):
	global CharRoom
	print "You are in {0} ({2}).\n{1} is here.".format(roomname,monster,CharRoom)
	print "There are exits:"
	for i in range(0,len(movechars)):
		m = movechars[i]
		print "{0} ({1})".format(DirName(m),m)

def DoMove(movechars,newrooms):
	global BATCH
	if BATCH:
		print"DoMove({0},{1})".format(movechars,newrooms)
		l=len(movechars)
		i = random.randrange(0,l)
		newroom = int(newrooms[i*2:i*2+2])
		return newroom
	
	print "Please pick a direction ({0}): ".format(movechars)
	m=raw_input().upper()
	for i in range(0,len(movechars)):
		if movechars[i]==m:
			return int(newrooms[i*2:i*2+2])
	print "That was not a valid move."
	print "So you get teleported."
	return random.randrange(0,9)+1
		
def DoAction(actdesc,actchars,winprc,winatt,giftsize,giftatt,bribeprc,bribeamt,monstname):
	# acctdesc - Description of the action 
	# actchars - charaters that identify the possible actions
	# winprc -- percentage (0-100) that you need to beat to win
	# winatt -- attribute you get to use to improve your roll
	# giftsize -- how much you get if you win
	# giftatt -- what attribute is advanced
	# bribeprc -- if bribing is allowed, what chance to succeed
	# bribeamt -- how much money you have to have to bribe
	# monstname -- who is this monster
	global AttributeEffect,BATCH
	
	if BATCH:
		print"DoAction({0},{1},{2},{3},{4},{5},{6},{7},{8})".format(actdesc,actchars,winprc,winatt,giftsize,giftatt,bribeprc,bribeamt,monstname)
	print "{0} wants to have a {1} with you. \nYou may:".format(monstname,actdesc)
	for i in range(0,len(actchars)):
		a=actchars[i]
		print "{0}: {1}".format(a,ActName(a))
	if BATCH:
		l=len(actchars)
		i = random.randrange(0,l)
		act = actchars[i:i+1]
		print "You took action: {0}.".format(act)
	else:		
		act = raw_input("What is your choice ({0}): ".format(actchars)).upper()
	roll = random.randrange(100)+AttributeEffect*GetAtt(winatt)
	giftname = AttName(giftatt)
	if act == "F":
		# "Fight"
		if roll>winprc:
			print "You won the {0} with {1}!".format(actdesc,monstname)
			print "You gain {0} from your {1}.".format(giftsize,giftname)
			ChangeAtt(giftatt,giftsize)
		else:
			print "You lost the {0} with {1}!".format(actdesc,monstname)
			print "You lost {0} from your {1}.".format(-giftsize,giftname)
			ChangeAtt(giftatt,-giftsize)
	elif act == "D":
		# "Drink"
		if roll>winprc:
			print "You won the {0} with {1}!".format(actdesc,monstname)
			print "You gain {0} from your {1}.".format(giftsize,giftname)
			ChangeAtt(giftatt,giftsize)
		else:
			print "You lost the {0} with {1}!".format(actdesc,monstname)
			print "You lost {0} from your {1}.".format(-giftsize,giftname)
			ChangeAtt(giftatt,-giftsize)
	elif act == "G":
		# "Gamble"
		if roll>winprc:
			print "You won the {0} with {1}!".format(actdesc,monstname)
			print "You gain {0} from your {1}.".format(giftsize,giftname)
			ChangeAtt(giftatt,giftsize)
		else:
			print "You lost the {0} with {1}!".format(actdesc,monstname)
			print "You lost {0} from your {1}.".format(-giftsize,giftname)
			ChangeAtt(giftatt,-giftsize)
	elif act == "R":
		# "Run"
		print "You succeeded in escaping!"
		print "You lose nothing but will be teleported randomly."
		return random.randrange(1,4)
	elif act == "B":
		# "Bribe"
		if roll>winprc:
			print "You won the {0} with {1}!".format(actdesc,monstname)
			print "You gain {0} from your {1}.".format(giftsize,giftname)
			ChangeAtt(giftatt,giftsize)
		else:
			print "You lost the {0} with {1}!".format(actdesc,monstname)
			print "You gain {0} from your {1}.".format(-giftsize,giftname)
			ChangeAtt(giftatt,-giftsize)

def DoRoom(roomno):
	RoomName =""
	MonName = ""
	MoveNames =""
	MoveDirs = ""
	
	if roomno == 0:
		RoomName = "Entrance Hall"
		MonName = "A Basilisk"
		MoveNames = "SEW"
		MoveDirs = "020301"
		actdesc = "craps game"
		actchars = "RGB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 1:
		RoomName = "Long Hall"
		MonName = "Bigfoot"
		MoveNames = "ES"
		MoveDirs = "0204"
		actdesc = "fight"
		actchars = "RFB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 2:
		RoomName = "Green Room"
		MonName = "Wendigo"
		MoveNames = "NEWS"
		MoveDirs = "00030105"
		actdesc = "drink"
		actchars = "RDB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 3:
		RoomName = "Dark Corridor"
		MonName = "Dracula"
		MoveNames = "WS"
		MoveDirs = "0206"
		actdesc = "poker game"
		actchars = "RGB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 4:
		RoomName = "Bright Bedroom"
		MonName = "A Skeleton"
		MoveNames = "NES"
		MoveDirs = "010507"
		actdesc = "beer pong"
		actchars = "RDB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 5:
		RoomName = "Trashed Kitchen"
		MonName = "An Ogre"
		MoveNames = "NEWS"
		MoveDirs = "02060408"
		
		actdesc = "arm wrestle"
		actchars = "RFB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 6:
		RoomName = "Dingy Den"
		MonName = "The Minotaur"
		MoveNames = "NWS"
		MoveDirs = "030509"
		actdesc = "betting on flies"
		actchars = "RGB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 7:
		RoomName = "Elegant Drawing Room"
		MonName = "An Imp"
		MoveNames = "NE"
		MoveDirs = "0408"
		
		actdesc = "boxing"
		actchars = "RFB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	elif roomno == 8:
		RoomName = "Black Parlor"
		MonName = "Death"
		MoveNames = "NEW"
		MoveDirs = "050907"
		actdesc = "dice"
		actchars = "RGB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
		
	elif roomno == 9:
		RoomName = "Dragon's Cave"
		MonName = "A Dragon"
		MoveNames = "NWS"
		MoveDirs = "060899"
		actdesc = "delver roast"
		actchars = "RFB"
		winprc = 30
		winatt = 3
		giftsize = 5
		giftatt = 6
		bribeprc = 20
		bribeamt = 7
	else:
		print "Illegal Room #",roomno
		print RoomName,MonName,MoveNames
		return 0
	
	DescribeRoom(RoomName,MonName,MoveNames)
	NewRoom = DoAction(actdesc,actchars,winprc,winatt,giftsize,giftatt,bribeprc,bribeamt,MonName)
	if NewRoom==None:
		NewRoom = DoMove(MoveNames,MoveDirs)
	return NewRoom

	
#**************************************** MAIN PROGRAM *******************************************
# This is a dungeon program
AttributeEffect = 0.33 #this determines how much affect attributes have; attribute*effect = what we add in 
# First we create a character. He has 5 atrributes: name, strength (1), size (2), luck (3), charisma (4). The user can enter the name and pick which of the 4 other attributes is important.

BATCH = False #this determines whether this is a test or a interactive run	

print "Welcome to Dave's Death Dungeon!"
print
print "Let me make a character for you."
print "Every character has 4 attributes."
print "\t1 Strength (Str) makes you a good fighter and, well, strong."
print "\t2 Size makes you tough and gives you stamina."
print "\t3 Luck makes you good at games of chance and at chance encounters."
print "\t4 Charisma (Chr) make you likeable and helps you talk your way out of things."

if BATCH:
	name = "Daweed" 
	boost = random.randrange(1,5)
	print "You picked #{0} ({1}).".format(boost,AttName(str(boost)))
else:
	name = raw_input("What is your name (keep it short)? ")
	print 
	boost = int(raw_input("Which attribute do you want to boost? (1234): "))

CreateChar(name,boost)
DescribeChar()

# Now we start the Character somewhere
#Special rooms: 
# 0: starting room (if you want to start somewhere else put a teleporter in room 0!)
# 99: Exit room, if you get here the game ends
DEAD_STAMINA = 0
EXIT_ROOM = 99
STARTING_ROOM = 0

CharRoom = STARTING_ROOM
while CharStamina>DEAD_STAMINA and CharRoom != EXIT_ROOM:
	CharRoom = DoRoom(CharRoom)

if CharStamina<=DEAD_STAMINA:
	print "Sorry you died!"
	DescribeChar()
else:
	print "Congrats you won!"
	DescribeChar()
if not BATCH:
	raw_input()


