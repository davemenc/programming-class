#This is based on the first stage of a Saturn V -- overkill I admit
ROCKET_WEIGHT = 131000 #KG
FUEL_WEIGHT = 2169000 #KG

THRUST = 34000000 #N
PAYLOAD = 100 # KG
FUEL_BURN = 12911 #KG/s
GRAVITY_ACC = 9.8#M/s -- gravity pulls you DOWN
ESCAPE_VEL = 11200

REPORT_INTERVAL = 20

Time = 0.0
Velocity = 0.0
Rocket_Mass = float(ROCKET_WEIGHT+PAYLOAD)
Acceleration = 0.0
Altitude = 0.0

Fuel_Mass = float(FUEL_WEIGHT)
print "Our Rocket is {0}kg with a thrust of {1}N and a payload of {2} kg and {3}kg of fuel which burns at {4} kg/s. ".format(ROCKET_WEIGHT,THRUST,PAYLOAD,FUEL_WEIGHT,FUEL_BURN)
print "Will we be able to overcome gravity ({0}m/s/s) and reach escape velocity ({1})? ".format(GRAVITY_ACC,ESCAPE_VEL)
print "Count down: 3,2,1 LIFTOFF!"
print
print "Time\tFuel\tAccerlation\tVelocity\tAltitude (km)"
while Altitude>=0 and Velocity<ESCAPE_VEL :
	Total_Mass = Rocket_Mass+Fuel_Mass
	if Fuel_Mass>FUEL_BURN:
		Acceleration = THRUST/Total_Mass
		Fuel_Mass += -FUEL_BURN
	else:
		Acceleration = (THRUST * Fuel_Mass/FUEL_BURN)/Total_Mass
		Fuel_Mass = 0
	Velocity += Acceleration - GRAVITY_ACC 
	Altitude += Velocity
	Time += 1.0
	if Time/REPORT_INTERVAL == int(Time/REPORT_INTERVAL):
		print "{0}\t{1}\t{2}\t{3}\t{4}".format(round(Time),round(Fuel_Mass),round(Acceleration),round(Velocity),round(Altitude/1000))

if Altitude < 0:
	print "Too bad, the rocket crashed! It was going {0} when it hit!".format(Velocity)
if Velocity > ESCAPE_VEL:
	print "Congratulations, we've reached escape velocity and we'll go into orbit! We still have {0} fuel left, too.".format(Fuel_Mass)


	
	
	
