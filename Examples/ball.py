print "Let's see how long a ball takes to hit the ground."
height = float(raw_input("How high does it start (m)?"))
time_inc = .01 # 100th of seconds
time = 0.0
acc = 0.0
vel = 0.0
while height>0:
    acc = acc+9.8*time_inc # acceleration due to gravity
    vel = vel + acc
    height = height - vel
    time = time +1
    print time*time_inc,vel,height

print "Your ball hit the ground after",time*time_inc, "seconds."
print "It was moving at ",vel,"meters per second."

    
