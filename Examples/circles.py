import pygame, sys
from pygame.locals import *
import random
import math
import time

# set up the window
pygame.init()
ScreenX = 1680
ScreenY = 1050
DISPLAYSURF = pygame.display.set_mode((ScreenX,ScreenY),pygame.FULLSCREEN)
#pygame.display.set_caption('Circles')
DISPLAYSURF.fill((255, 255, 255))

# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)
CYAN =  (  0, 255, 255)
MAGENTA =(255,  0, 255)
YELLOW=(255,255,0)
ORANGE =(255,  69,   0)
AQUAMARINE = (102,205,170)
NAVY = (0,0,128)
BLUEVIOLET = (138,43,226)
DEEPPINK=(255,20,147)
OLIVEGREEN= (85,107,47)
YELLOWGREEN=(154,205,50)
colors = [ RED, GREEN, BLUE,CYAN,MAGENTA,ORANGE,AQUAMARINE,NAVY,BLUEVIOLET,DEEPPINK,WHITE,YELLOW,OLIVEGREEN,YELLOWGREEN]

# set drawing controls
DRAWINGS=2000
TotalDraws = 0
FirstX = random.randrange(100,ScreenX-100)
FirstY = random.randrange(100,ScreenY-100)

# draw a bunch of things
while TotalDraws< DRAWINGS or DRAWINGS==0:
	TotalDraws+=1
	color = colors[random.randrange(len(colors))]
	centerX = random.randrange(100,ScreenX-100)
	centerY = random.randrange(100,ScreenY-100)
	radius = random.randrange(10,300)

	pygame.draw.circle(DISPLAYSURF,color,(centerX,centerY),radius,1)
#	pygame.draw.line(DISPLAYSURF,color,(FirstX,FirstX),(centerX,centerY),1)
	pygame.display.update()

# wait a bit...
time.sleep(3)

# ... and exit.
pygame.quit()

