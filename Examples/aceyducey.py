import random
STAKE = 10 #this is how many chips you start with
PLAYS = 10 #this is how many bets you may place
MAXBET = 10 #this is the maximum you can bet
MINBET = 1 #since the bet sucks it would be good to be able to bet 0; I'm against it
print "Let's play Aceducey!" 

print "I will draw two cards and then you will bet from 1 to 10 chips"
print "on whether the next card will be between those two cards. "
print "If it is you win otherwise you lose.I win ties.\n"
print "You get 10 tries unless you run out of chips first. "
print "Your score is how many chips you have left. "
print "The maximum you COULD have would be 110. How close can you come!?\n"
cardnames = "Ace  Two  ThreeFour Five Six  SevenEightNine Ten  Jack QueenKing "
cardsuits = " of Clubs    of Diamonds of Hearts   of Spades  "
cardNameLen = 5
cardSuitLen = 12
nameCount = 13
suitCount = 4
#print "Suit len should be {0} and it is {1}.".format(suitCount*cardSuitLen,len(cardsuits))
#print "name len should be {0} and it is {1}.".format(cardNameLen*nameCount,len(cardnames))
#for suit in range(0,suitCount):
#	print "suit {0} '{1}' which is {2}:{3}".format(suit,cardsuits[suit*cardSuitLen:cardSuitLen+suit*cardSuitLen],suit*cardSuitLen,cardSuitLen)
#for name in range(0,nameCount):
#	print "name {0} '{1}' which is {2}:{3}".format(name,cardnames[name*cardNameLen:cardNameLen+name*cardNameLen],name*cardNameLen,cardNameLen+name*cardNameLen)

chips = STAKE
wins = 0
losses = 0
for bet in range(0,PLAYS):
	if chips <= 0: #you lost
		break;
	card1 = random.randrange(0,nameCount)
	card2 = random.randrange(0,nameCount)
	if card1>card2: # swap the cards so card 1 is always less than card 2
		c = card1
		card1 = card2
		card2 = c
	card3 = random.randrange(0,nameCount)
	suit1 = random.randrange(0,suitCount)
	suit2 = random.randrange(0,suitCount)
	suit3 = random.randrange(0,suitCount)
	while (card3 == card1 and suit3==suit1) or (card3 == card2 and suit3==suit2) or (card1==card2 and suit1==suit2) : # we have 2 of the same card!
		#so pick new suits (this is a kind of a bad hack but should work every time)
		suit1 = random.randrange(0,suitCount)
		suit2 = random.randrange(0,suitCount)
		suit3 = random.randrange(0,suitCount)
		
		
	cardname1 = cardnames[card1*cardNameLen:cardNameLen+card1*cardNameLen].strip()+" "+cardsuits[suit1*cardSuitLen:cardSuitLen+suit1*cardSuitLen].strip()
	cardname2 = cardnames[card2*cardNameLen:cardNameLen+card2*cardNameLen].strip()+" "+cardsuits[suit2*cardSuitLen:cardSuitLen+suit2*cardSuitLen].strip()
	cardname3 = cardnames[card3*cardNameLen:cardNameLen+card3*cardNameLen].strip()+" "+cardsuits[suit3*cardSuitLen:cardSuitLen+suit3*cardSuitLen].strip()
	print
	print "I play {0} and {1} \nYou have to get a card between these to win! ".format(cardname1,cardname2)
	print
	maxbet = min(MAXBET,chips)
	bet = 0
	while bet<MINBET or bet >maxbet:
		bet = int(raw_input("Bet ({0}-{1})".format(MINBET, maxbet)))
		#print "Bet ({0}-{1})".format(MINBET, maxbet)
		#bet = MINBET
		if bet<MINBET or bet>maxbet:
			print "You have to bet between {0} and {1} but you bet {2}. Bet again, please.".format(MINBET,maxbet,bet)
			print
	print "You bet {0} and drew the {1}.".format(bet,cardname3)
	print
	if card1<card3 and card2>card3:
		chips += bet
		print "Since {0} is between {1} and {2} you win!. \nYou now have {3} chips.".format(cardname3, cardname1, cardname2,chips)
		print
		wins+=1
	else:
		chips+=-bet
		print "I'm sorry, {0} is not between {1} and {2}. \nYou now have {3} chips.".format(cardname3,cardname1,cardname2,chips)
		print
		losses+=1
print
print "Game over. That was fun! Your final total was {0}. You won {1} and lost {2}.".format(chips,wins,losses)
print
print "Please come again!"
print
raw_input("Please press enter to quit")