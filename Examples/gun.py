import math
import random
# 400 m/s muzzle velocity
#0-60 degrees
#16000 m range
#9.8 m/s/s y
#error around 4m
def CalcHit(power,angle):
	#print "CalcHit({0},{1})".format(power,angle)

	TIMESLICE = 1.0/1000.0
	FULLPOWER = 400
	muzzle = FULLPOWER*power/100
	
	vely = math.sin(math.radians(angle))*muzzle
	velx = math.cos(math.radians(angle))*muzzle
	#print "velx={0},vely={1}, muzzle={2}".format(velx,vely,muzzle)
	x = 0.0
	y = 1
	t=0.0
	while y>0:
		lastx=x
		lasty=y
		y+=vely*TIMESLICE
		x+=velx*TIMESLICE
		vely=vely-9.8*TIMESLICE
		t+=TIMESLICE
	return (lastx+x)/2.0

TRIES=5
RADIUS = 50
targetx = random.randrange(1000,15000) # target will be 1000 to 15000 meters away
print "Welcome to GUN, the artilery game!"
print "There is a target {0} meters down range. You need to select a power (1-100%) and an angle (2-60 degrees) that will hit it.".format(targetx)
print "You will have {0} tries to hit it. If you get within {1} meters, you win! Otherwise you lose.".format(TRIES,RADIUS)
for i in range(0,5):
	print "Shot #{0}.".format(i+1)
	power = float(raw_input("How much power (1-100) do you want? "))
	if power>100:
		power=100
	if power<1:
		power=1
	angle = float(raw_input("What angle (2-60) do you want? "))
	if angle>60:
		angle=60.0
	if angle<2:
		angle = 2.0
	shot = CalcHit(power,angle)
	if shot>targetx:
		result = "long"
	else:
		result = "short"
	off = math.fabs(targetx-shot)
	print "You fired with power {0} at an angle of {1}.".format(power,angle)
	if off <= RADIUS:
		print "\nCongratulations, YOU GOT IT! Your shot went {0} meters and came within {1} of the target ({2}).".format(shot,off,targetx)
		break
	else:
		print "\nYou missed.\nYour shot landed at {0} which is {1} meters {2} You have {3} tries left. ".format(shot, off,result,TRIES-i-1)
		if TRIES-i-1<1:
			print "I'm sorry, you lost!\n"

raw_input("Press return to end game.")