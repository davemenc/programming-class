#Looping
#the while loop
print "First we'll demonstrate a while loop."
x = 0
while x<5:
    print x
    x=x+1
print "While loop runs until conditional is false"
print "Why does it not print the 5?"
print "Caution: if it's never false it will run forever. I'd love to show you an example but the program will never end and you probably have other plans for your life..."
print

print "Now we'll demonstrate a for loop."

for x in [0,1,2,3,4]:
    print x

print "For loops do what the limited while loop did but runs across a list."
print "range(first,last) generates a list you can run across.",range(0,5)
print "It vexes me as a teacher that range(0,5) goes from 0-4. But that's how it works and you'll have to learn to love it."
print
print "While is much more flexible than for but for can be really quick and easy to use."
