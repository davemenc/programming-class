#functions
print "We've seen a few functions already: raw_input() and range()."
print "Notice that they both have parenthesis after them '()'."
print range(0,5)
print "Here is where I blow your mind...."
print
func = range
print func(0,5)
print "What just happened? Well, 'range' is a thing and you can assign a thing to a variable."
print "And then we put the parentheses after the variable and python figures it out."
print "I think this is weird and confusing except for certain special cases but some people just LOVE this stuff."

