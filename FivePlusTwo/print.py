print "Hello World"
print 2+4
print "Dave",2+4,"this is a string"
print "strings have quotes around them (mostly)"
print "commas","put things","on the same line"
print
print "print by itself introduces a blank line"
print "if a comma is the last thing, it doesn't",
print "give you a new line at all"
print "Print is useful at this point because it's hard to know what a program is doing without print"
