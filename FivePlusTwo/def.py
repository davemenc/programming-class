#creating a function "DEF"
print "Python has HUGE libraries of stuff you can use without writing your own."
print "But you want to write your own to be organized."
def madlib(adj1,adj2,noun1,verb,adj3,noun2):
    print "The",adj1,adj2,noun1,verb,"over the",adj3,noun2,"."
madlib("quick","brown","fox","jumped","lazy","dog")

madlib("flighty","stupid","paint brush","painted","briliant","canvas")
madlib("spotty","sore","toast","invented","electric","monastery")
       
print
print "Functions can also return a value"
def mypower(val,power):
    result = 1
    count = 0
    while count<power:
        result = result*val
        count += 1 #+= means "take the left side, add the right side to it, put it back in the left side"
    return result
    
print mypower(2,3)
print mypower(4,5)
