#progam to demonstrate assignment = ("gets")
#by the way, this is a comment
#anything after the "#" is ignored
print '= ("gets") assigns something to a variable'
left_side = "The left side of the = has the variable name"
right_side = "The right side of the = has the expression: what will be in the variable"
variable = "What kind of variable it is depends on the right side."
print left_side
print right_side
print variable
print
first_num = 4
second_num = 6
sum = first_num+second_num
print first_num,"+",second_num,"=",  sum
print

string1 = "Dave"
string2 = "likes ice cream."
print string1,string2
print

print "we can add two numbers but can we add two strings? "
print "Yes we can"
string3 = string1+string2
print string3
print 'notice that the , adds a space but the plus does not so we get "davelikes" instead of "dave likes"'
string3 = string1+" "+string2
print string3
print "all fixed."
print
print "A few things about strings. You can use single or double quotes which is mainly so you can put single quotes into double quotes."
print "And vice versa."
print "The value you assign to a variable has to be in quotes. That's called a 'literal' because means literally what it says."
print "When you just print out the variable, it doesn't need the quotes."
print "string3",string3
