#examples for raw_input()
print "raw input gets data from you, the user"
name = raw_input("What is your name? ")
# notice the space I put ...........^ here; try it without that space
print "Your name is",name
print
print "raw_input always get a string but we can change it to an int if we want to do arithmatic"
first_value = raw_input("Enter a number, please. ")
second_value = raw_input("Enter a second number please. ")
print first_value+second_value
print
print int(first_value)+int(second_value)
print
print "Note that '+' does different things for different types of variables."

