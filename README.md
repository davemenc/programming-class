# README #

This is an outline for a class. The purpose of the class is to acquaint the student with what it would be like to be a programmer. This won't teach them to BE a programmer in any but the most technical sense (yes, by the end they should be able to write rudimentary Python scripts). But they will know if they want to pursue further studies to become more knowledgeable. 

I assert that, for the vast majority of people, knowing if they want to pursue programming would be valuable. Obviously if a person already programs or has every been exposed to programming they know the degree to which they want to pursue this skill set. And some people know themselves well enough to know that they simply aren't interested. But for everyone else -- practically everyone in the world -- a quick, cheap introduction would by helpful.

I am a bit self conscious about my first class because anyone who knows about programming will immediately see that it's not much of an introduction. I imagine my fellow programmers pointing out that I left a bunch of stuff out. But before you tell me that consider this: I think it's TOO long. So I'm only going to consider changes that shorten it or keep it the same length. 

### What is this repository for? ###

Outline of a class; eventually it will probably be in the form of a slide set. What's the right tool for making open source slides?


### Contribution guidelines ###

I would love to hear about how to make this class more focused but I also want to do some other classes so all suggestions are welcome. 

But if you really want to get your hands dirty, some examples and some assignments ("assignments" are examples that we describe but don't show them; "examples" are assignments that you go through in class) would be very helpful. And you can't have too many, can you? Ideally they should be very simple and straightforward. 

### Who do I talk to? ###

Dave Menconi
dave at menconi dot com

### License Information ###
Copyright Dave Menconi, 2015
The files, class outlines, videos, source code, slide sets and other materials are hearby released under the apache license, v2.
https://www.apache.org/licenses/LICENSE-2.0
This software is releases without warranty of any kind. Use it as you see fit but on your head be it if you hurt yourself with it (not that this is even possible).